TEMPLATE = lib
CONFIG += static exceptions
CONFIG -= debug_and_release

TARGET = GoogleTest

INCLUDEPATH += \
    googletest/googletest/include \
    googletest/googlemock/include \
    googletest/googletest \
    googletest/googlemock

SOURCES = \
    googletest/googletest/src/gtest-all.cc \
    googletest/googlemock/src/gmock-all.cc

DESTDIR = $$PWD/lib
OBJECTS_DIR = $$PWD/.obj
MOC_DIR = $$PWD/.moc
RCC_DIR = $$PWD/.rcc
UI_DIR = $$PWD/.ui
